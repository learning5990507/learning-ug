import { NextPageContext } from "next";
import axios from "axios";
import { PersonType } from "@/helpers/types/people";
import { PersonTableInfo } from "@/Components";

type PersonTypeData = {
	data: PersonType;
}

export default function Person({ data }: PersonTypeData) {
	return (
		<PersonTableInfo { ...data } />
	);
}

Person.getInitialProps = async ({ query }: NextPageContext) => {
	const { id } = query;

	try {
		const response = await axios.get(`https://swapi.dev/api/people/${ id }`);
		const data: {
			name: string,
			gender: string,
			eye_color: string,
			birth_year: string,
			hair_color: string,
			height: string
		} = response.data;

		return {
			data: {
				name: data.name,
				gender: data.gender,
				eye_color: data.eye_color,
				birth_year: data.birth_year,
				hair_color: data.hair_color,
				height: data.height
			},
		};
	} catch (error) {
		return {
			error: true,
		};
	}
};
