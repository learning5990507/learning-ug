import axios from "axios";
import { InitialPersonType, PeopleResponseType } from "@/helpers/types/people";
import { CharacterCard, Pagination } from "@/Components";
import { NextPageContext } from "next";
import Link from "next/link";

type DataType = {
	pagesCount: number;
	data: InitialPersonType[];
	error?: boolean;
}

export default function People({ pagesCount, data, error }: DataType) {

	if (error) {
		return (
			<>
				<h1>Poza zakresem stron</h1>
				<Link href="/people">
					Wróć do pierwszej strony
				</Link>
			</>
		)
	}

	return (
		<>
			<ul>
				{ data.map((person, index) => (
					<CharacterCard key={ index } { ...person } />
				)) }
			</ul>

			<div>
				<Pagination pagesCount={ pagesCount } />
			</div>
		</>
	);
}

People.getInitialProps = async ({ query }: NextPageContext) => {
	const page = query.p ?? 1;

	try {
		const response = await axios.get(`https://swapi.dev/api/people?page=${ page }`);
		const data: PeopleResponseType = response.data;
		const pagesCount = Math.ceil(response.data.count / 10)

		return {
			pagesCount,
			data: data.results?.map((obj: InitialPersonType)=>{
				return {
					name: obj.name,
					gender: obj.gender,
					eye_color: obj.eye_color,
					url: obj.url
				}
			})
		};
	} catch (error) {
		return {
			error: true,
		};
	}
};
