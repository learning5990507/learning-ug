import styled from "styled-components";

export const Container = styled.div`
	padding: 8px;
	border-radius: 4px;
	display: flex;
	flex-direction: column;
	gap: 4px;
	background-color: white;
	max-width: 400px;
	margin: 4px;
	color: black;
`;

export const Header = styled.div``

export const Name = styled.h2``

export const Details = styled.div``
