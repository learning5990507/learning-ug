import * as CharacterCardStyle from './CharacterCard.styled';
import { InitialPersonType } from "@/helpers/types/people";
import Link from "next/link";

const CharacterCard = (props: InitialPersonType) => {
	const { name, gender, eye_color, url } = props;

	return (
		<Link href={{
			pathname: `/people/${url.split("/").slice(-2)[0]}`,
		}}>
			<CharacterCardStyle.Container>
				<CharacterCardStyle.Header>
					<CharacterCardStyle.Name>
						{ name }
					</CharacterCardStyle.Name>
				</CharacterCardStyle.Header>
				<CharacterCardStyle.Details>
					<ul>
						<li>Gender: { gender }</li>
						<li>Eye color: { eye_color }</li>
					</ul>
				</CharacterCardStyle.Details>
			</CharacterCardStyle.Container>
		</Link>
	)
};

export default CharacterCard;
