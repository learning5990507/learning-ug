import { styled } from "styled-components";

export const Body = styled.tbody`
	
`;

export const TR = styled.tr`
    border: 1px solid #ddd;
    padding: 8px;
	
`;

export const TH = styled.th`
    border: 1px solid #ddd;
    padding: 8px;
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #04AA6D;
    color: white;
`;
