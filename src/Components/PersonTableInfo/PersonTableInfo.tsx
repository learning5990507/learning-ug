import * as Table from './PersonTableInfo.styled';
import { PersonType } from "@/helpers/types/people";

const PersonTableInfo = (data: PersonType) => {
	const { name, gender, eye_color, birth_year, hair_color , height} = data;

	return (
		<table>
			<Table.Body>
				<Table.TR>
					<Table.TH>Name:</Table.TH>
					<Table.TH>{ name }</Table.TH>
				</Table.TR>
				<Table.TR>
					<Table.TH>Birth year:</Table.TH>
					<Table.TH>{ birth_year }</Table.TH>
				</Table.TR>
				<Table.TR>
					<Table.TH>Eye color:</Table.TH>
					<Table.TH>{ eye_color }</Table.TH>
				</Table.TR>
				<Table.TR>
					<Table.TH>Gender:</Table.TH>
					<Table.TH>{ gender }</Table.TH>
				</Table.TR>
				<Table.TR>
					<Table.TH>Hair color:</Table.TH>
					<Table.TH>{ hair_color }</Table.TH>
				</Table.TR>
				<Table.TR>
					<Table.TH>Height:</Table.TH>
					<Table.TH>{ height }</Table.TH>
				</Table.TR>
			</Table.Body>
		</table>
	)
}

export default PersonTableInfo;
