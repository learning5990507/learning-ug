import { styled } from "styled-components";

export const List = styled.div`
	display: flex;
	gap: 4px;
	padding: 4px;
	color: black;
`;

export const Item = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	height: 30px;
	width: 30px;
	background-color: white;
	border-radius: 4px;
	
	
	&:hover{
		background-color: lightgray;
		cursor: pointer;
	}
`;
