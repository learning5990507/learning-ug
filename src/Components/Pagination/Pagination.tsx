import * as PaginationStyle from './Pagination.styled';
import Link from "next/link";

const Pagination = ({ pagesCount }: { pagesCount: number }) => {
	let pages: number[] = [];

	for (let i = 1; i <= pagesCount; i++) {
		pages.push(i);
	}

	return (
		<PaginationStyle.List>
			{ pages.map((page) => (
				<Link href={`/people?p=${page}`} key={ page }>
					<PaginationStyle.Item>
						{ page }
					</PaginationStyle.Item>
				</Link>
			)) }
		</PaginationStyle.List>
	);
};

export default Pagination;
