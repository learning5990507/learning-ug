export const BASE_API_URL = "https://swapi.dev/api/"

export const POSSIBLE_LINKS = {
	FILMS: "films",
	PEOPLE: "people",
	PLANTES: "planets",
	SPECIES: "species",
	STARSHIPS: "starships",
	VEHICLES: "vehicles",
}
