export type PeopleResponseType = {
	count: number;
	next: string;
	previous: null;
	results: InitialPersonType[];
};

export type InitialPersonType = {
	name: string;
	gender: string;
	eye_color: string;
	url: string;
}

export type PersonType = {
	birth_year: string;
	created: string;
	edited: string;
	eye_color: string
	films: string[];
	gender: string;
	hair_color: string;
	height: string;
	homeworld: string;
	mass: string;
	name: string;
	skin_color: string;
	species: string[];
	starships: string[];
	url: string;
	vehicles: string[];
}
